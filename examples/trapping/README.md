# Radiation damage in pixel detectors: example of trapping of charges

**Work in progress**
This example is similar to the example in `example.conf`, with trapping applied to the two detectors under test.
After radiation damage to the bulk of a silicon sensor, charge carriers can get trapped and no longer propagate in a sensor, so that they no longer contribute to charge integration.
In this example, a neutron equivalent fluence of $`\Phi_{\mathrm{eq}} = 5 \cdot 10^{15}`$ / cm$`^2`$,  $`\Phi_{\mathrm{eq}} = 1 \cdot 10^{15}`$ / cm$`^2`$,  and constant trapping parameters are assumed for detectors 1 and 2, respectively.

The goal of this setup is to simulate the performance of sensors after radiation damage with a simple model of charge carrier trapping. The user can alter the trapping parameters, fluence, and choose to use trapping dependent on temperature and the electric field using the configuration parameters of the  `GenericPropagation` module. See the documentation for this module for more details.

In this example, a linear electric field is applied; however, external electric fields can also be applied by changing the configuration of the `ElectricFieldReader` module to take into account the correct electric field after radiation damage.

